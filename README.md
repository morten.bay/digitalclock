# Digital Clock

A concept App showing a digital clock component with dynamic updates, created in Angular with Tailwind CSS.

## Installation

Clone the project using Git.

```
git clone git@gitlab.com:morten.bay/digitalclock.git
```

## Dependencies

```
Node.js LTS - https://nodejs.org/en/
Angular CLI - https://angular.io/cli
```

## Usage

Install the node dependencies:

```
npm install
```

Run the project:

```
ng serve
```

Navigate to 'http://localhost:4200' in your browser.

## Example

![example](src/example.JPG)

## Created by

[Morten Bay Nielsen (@morten.bay)](@morten.bay)

## License

[MIT](https://choosealicense.com/licenses/mit/)
