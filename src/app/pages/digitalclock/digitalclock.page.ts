import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { registerLocaleData } from '@angular/common';
import { DatePipe } from '@angular/common';
import localeDa from '@angular/common/locales/da';
registerLocaleData(localeDa);

@Component({
  selector: 'app-digitalclock',
  templateUrl: './digitalclock.page.html',
  styleUrls: ['./digitalclock.page.css'],
})
export class DigitalclockPage implements OnInit, OnDestroy {
  // Setup the clock to use the Danish Locale.
  datePipe: DatePipe = new DatePipe('da-DK');

  // Create subscription for dynamic / timed updates.
  clockSubscription!: Subscription;

  // Setup display variables.
  time!: string | null; // Displaying the current time in the format: 12:00:00.
  dateNumeric!: string | null; // Displaying the current date in the format: 02 / 11 / 2022
  dateAbbreviated!: string | null; // Displaying the current date in the format: 2 Nov 2022 [Ons]

  constructor() {}

  ngOnInit(): void {
    // Initiate the clockSubscription using the RxJS Timer
    this.clockSubscription = timer(0, 1000)
      .pipe(
        map(() => new Date()),
        share()
      )
      .subscribe((dateTime) => {
        // format the dateTime values and update the observables
        this.dateNumeric = this.datePipe.transform(dateTime, 'dd / MM / yyyy');
        this.dateAbbreviated = this.datePipe.transform(dateTime, 'd MMM yyyy [ccc]');
        this.time = this.datePipe.transform(dateTime, 'HH:mm:ss');
      });
  }

  ngOnDestroy() {
    // Remove the clockSubscription on Component Destroy.
    if (this.clockSubscription) {
      this.clockSubscription.unsubscribe();
    }
  }
}
