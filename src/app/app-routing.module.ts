import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DigitalclockPage } from './pages/digitalclock/digitalclock.page';

const routes: Routes = [
  {
    path: '',
    component: DigitalclockPage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
